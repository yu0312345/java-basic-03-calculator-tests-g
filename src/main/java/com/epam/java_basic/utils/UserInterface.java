package com.epam.java_basic.utils;

import java.util.Scanner;

public final class UserInterface {

    private static final Scanner SCANNER = new Scanner(System.in);

    private UserInterface() {

    }

    public static double askDouble(String message) {
        System.out.println(message);
        double result = SCANNER.nextDouble();
        SCANNER.nextLine();
        return result;
    }

    public static String askLine(String message) {
        System.out.println(message);
        return SCANNER.nextLine();
    }

    public static String askWord(String message) {
        System.out.println(message);
        String result = SCANNER.next();
        SCANNER.nextLine();
        return result;
    }

    public static boolean askYesNo(String message) {
        System.out.println(message);
        String result = SCANNER.next();
        SCANNER.nextLine();
        return "Y".equalsIgnoreCase(result);
    }

    public static void print(String message) {
        System.out.println(message);
    }

}
